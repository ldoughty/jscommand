import {assert as assert} from 'chai';
import { AnonymousCommand } from '../src/entities/anonymousCommand';

describe('Command', () => 
{  
    it('execute()', async () =>
    {   
        assert.equal(await new AnonymousCommand(()=>2).execute(), 2);
    });
    it('revert()', async () =>
    {   
        assert.equal(await new AnonymousCommand(()=>2, ()=>3).revert(), 3);
    });
});

