import {assert as assert} from 'chai';
import { Command } from '../src/entities/command';

class TestCommand extends Command 
{
    action(data?: any):any {
        return data;
    }
}



class AsyncCommand extends Command
{
    async action(): Promise<number>{
        return new Promise<number>((resolve)=>{
            setTimeout(()=>resolve(3), 10);
        });
    }
}

describe('Command', () => 
{  
    it('execute()', async () =>
    {   
        assert.equal(await new TestCommand().execute(), undefined);
        assert.equal(await new TestCommand().execute(1), 1);
        assert.deepEqual(await new TestCommand().execute(['string']), ['string']);
    });

    it('execute() - work with a promise', async () =>
    {   
        let result = await new AsyncCommand().execute();
        assert.equal(result, 3);
    });
});

