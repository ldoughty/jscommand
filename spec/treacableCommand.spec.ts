import {assert as assert} from 'chai';
import { Command } from '../src/entities/command';
import { RevertableCommand } from '../src/entities/revertableCommand';
import { TraceableCommand } from '../src/entities/traceableCommand';

class TestCommand extends Command 
{
    action(data?: any):any {
        return data;
    }
}

class AsyncCommand extends Command
{
    async action(): Promise<number>{
        return new Promise<number>((resolve)=>{
            setTimeout(()=>resolve(3), 10);
        });
    }
}

class TestRevertCommand extends RevertableCommand 
{
    action(data?: any):any {
        return data;
    }
    revert(data?: any): any
    {
        return data+data;
    }
}

describe('TraceableCommand', () => 
{  
    it('execute()', async () =>
    {   
        assert.equal(await new TraceableCommand(new TestCommand()).execute(), undefined);
    });

    it('execute() - work with a promise', async () =>
    {   
        assert.equal(await new TraceableCommand(new AsyncCommand()).execute(), 3);
    });
    it('revert()', async () =>
    {   
        assert.equal(await new TraceableCommand(new TestRevertCommand()).revert(2), 4);
    });
});

